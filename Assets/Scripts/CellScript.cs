﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CellScript : MonoBehaviour {

    // Use this for initialization
    int myX;
    int myY;
    public bool life = false;

	void Start () {
        myX = (int)gameObject.GetComponent<RectTransform>().position.x;
        myY = (int)gameObject.GetComponent<RectTransform>().position.y;
        StartCoroutine(LifeActivity());
	}
	
     IEnumerator LifeActivity()
     {
        while(true)
        {
            yield return new WaitForSeconds(0.1f);
            int live = 0;
            //左
            if(myX - 1 >= 0)
            {
                if (CreateCells.cells[myX - 1, myY].GetComponent<CellScript>().life == true)               
                    live++;              
            }
            //右
            if (myX + 1 < CreateCells.xCount)
            {
                if (CreateCells.cells[myX + 1, myY].GetComponent<CellScript>().life == true)               
                    live++;              
            }
            //下
            if (myY - 1 >= 0)
            {
                if (CreateCells.cells[myX, myY - 1].GetComponent<CellScript>().life == true)
                    live++;
            }
            //上
            if (myY + 1 < CreateCells.yCount)
            {
                if (CreateCells.cells[myX, myY + 1].GetComponent<CellScript>().life == true)
                    live++;
            }
            //左上
            if (myX - 1 >= 0 && myY + 1 < CreateCells.yCount)
            {
                if (CreateCells.cells[myX - 1, myY + 1].GetComponent<CellScript>().life == true)
                    live++;
            }
            //右上
            if (myX + 1 < CreateCells.xCount && myY + 1 < CreateCells.yCount)
            {
                if (CreateCells.cells[myX + 1, myY + 1].GetComponent<CellScript>().life == true)
                    live++;
            }
            //左下
            if (myX - 1 >= 0 && myY - 1 >= 0)
            {
                if (CreateCells.cells[myX - 1, myY - 1].GetComponent<CellScript>().life == true)                
                    live++;
            }
            //右下
            if (myX + 1 < CreateCells.xCount && myY - 1 >= 0)
            {
                if (CreateCells.cells[myX + 1, myY - 1].GetComponent<CellScript>().life == true)                
                    live++;
            }

            //遊戲規則
            if (live == 0 || live == 1)//死於孤獨
                ChangeLife(false);
            else if (live == 2 || live == 3)//存活
                ChangeLife(true);            
            else if (live >= 4)//死於擁擠
                ChangeLife(false);          
        }
    }

    public void ChangeLife(bool flag)
    {
        if(flag)
        {
            gameObject.GetComponent<Image>().color = new Color(0, 0, 0);
            life = true;
        }
        else
        {
            gameObject.GetComponent<Image>().color = new Color(255, 255, 255);
            life = false;
        }
    }

}
