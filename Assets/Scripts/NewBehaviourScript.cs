﻿
public class NewBehaviourScript
{
    public int[] TwoSum(int[] nums, int target)
    {
        for (int i = 0; i < nums.Length; i++)
            for (int j = 0; j < nums.Length; j++)
                if (i != j)
                    if (nums[i] + nums[j] == target)
                        return new int[] { i, j };   
        return null;
    }

    public int Reverse(int x)
    {
        string flag = "";
        char[] carArray = x.ToString().ToCharArray();

        if (x < 0)
        {
            flag = "-";
            for (int i = carArray.Length; i >= 0; i--)
                flag += carArray[i - 1];
        }
        else if (x > 0)
        {
            for (int i = carArray.Length; i >=0; i--)
                flag += carArray[i - 1];
        }
        else
            flag = "0";


        return int.Parse(flag);
    }



    public int Reverse2(int x)
    {
        int intFlag = 10;
        int value = 0;

        int valueFlag = x;
        while(true)
        {
            if (intFlag <= valueFlag)
            {
                value = (value * 10) + (x % valueFlag);
                valueFlag = valueFlag * 10;
            }
            else if(x < 10 && x >-10)
            {
                value = x;
                break;
            }
            else
                break;          
        }
        return value;
    }

}
