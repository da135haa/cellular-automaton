﻿using UnityEngine;
using System.Collections;

public class CreateCells : MonoBehaviour {

    public static GameObject[,] cells;

    //範圍大小
    public static int xCount = 100;
    public static int yCount = 100;

    public GameObject view;

    // Use this for initialization
    void Start () {
        cells = new GameObject[xCount, yCount];

        for(int x=0;x< xCount; x++)
        {
            for (int y = 0; y < yCount; y++)
            {
                GameObject gb = (GameObject)Instantiate(Resources.Load("Cell"), new Vector3(x, y), Quaternion.identity);
                gb.transform.transform.SetParent(view.transform);
                cells[x, y] = gb;
            }
        }
  
        CreateOriginCells();
    }

    //建立初始值細胞的位置     
    void CreateOriginCells()
    {
        cells[32, 32].GetComponent<CellScript>().ChangeLife(true);
        cells[31, 31].GetComponent<CellScript>().ChangeLife(true);
        cells[30, 30].GetComponent<CellScript>().ChangeLife(true);
        cells[31, 29].GetComponent<CellScript>().ChangeLife(true);
        cells[31, 30].GetComponent<CellScript>().ChangeLife(true);
    }
	   
}
